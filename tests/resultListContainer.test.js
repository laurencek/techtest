import React from 'react';
import { mount } from 'enzyme';
import Renderer from 'react-test-renderer';

import TestData from './data';

import ResultListContainer from '../src/controls/resultList/resultListContainer';
import ResultList from '../src/controls/resultList/resultList';
import Pagination from '../src/controls/resultList/pagination';
import MovieRenderer from '../src/renderers/movie';

describe("ResultListContainer", () => {
	let props;
	let mountedResultListContainer;
	const resultListContainer = () => {
		if (!mountedResultListContainer) {
			mountedResultListContainer = mount(
				<ResultListContainer {...props} />
			);
		}
		return mountedResultListContainer;
	}
	
	beforeEach(() => {
		props = {
			doChangePage: () => null,
			emptyString: "No results",
			items: [],
			page: 1,
			renderer: {},
			total: 0,
			totalPages: 1
		};
		mountedResultListContainer = undefined;
	});

	it('ResultListContainer renders correctly when no results', () => {
		let tree = Renderer.create(<ResultListContainer {...props} />).toJSON();
		expect(tree).toMatchSnapshot();
	});
	
	it("ResultListContainer renders a ResultList", () => {
		expect (resultListContainer().find(ResultList).length).toBe(1);
	});

	it("ResultListContainer renders the empty text", () => {
		expect (resultListContainer().find("span.result-list-empty").length).toBe(1);
		expect (resultListContainer().find("span.result-list-empty").html()).toMatch(props.emptyString);
	});

	it("ResultListContainer renders no pagination", () => {
		expect (resultListContainer().find(Pagination).length).toBe(0);
	});

	describe("When items are passed", () => {
		beforeEach(() => {
			props = {
				doChangePage: () => null,
				emptyString: "No results",
				items: TestData.results,
				page: 1,
				renderer: new MovieRenderer(TestData.configuration),
				total: 3,
				totalPages: 1
			}
			mountedResultListContainer = undefined;
		});

		it('ResultListContainer renders correctly with results', () => {
			let tree = Renderer.create(<ResultListContainer {...props} />).toJSON();
			expect(tree).toMatchSnapshot();
		});

		it("ResultListContainer renders a ResultList", () => {
			expect (resultListContainer().find(ResultList).length).toBe(1);
		});

		it("ResultListContainer renders a ResultList of Movies", () => {
			expect (resultListContainer().find(".movie-title").length).toBe(3);
		});
	
		it("ResultListContainer renders pagination", () => {
			expect (resultListContainer().find(Pagination).length).toBe(1);
		});
	});
})



