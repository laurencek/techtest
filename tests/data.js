const TestData = {
    configuration: {
        "images":{
            "base_url":"http://image.tmdb.org/t/p/",
            "secure_base_url":"https://image.tmdb.org/t/p/",
            "backdrop_sizes":["w300","w780","w1280","original"],
            "logo_sizes":["w45","w92","w154","w185","w300","w500","original"],
            "poster_sizes":["w92","w154","w185","w342","w500","w780","original"],
            "profile_sizes":["w45","w185","h632","original"],
            "still_sizes":["w92","w185","w300","original"]
        },
        "change_keys":["adult","air_date","also_known_as","alternative_titles","biography","birthday","budget","cast","certifications","character_names","created_by","crew","deathday","episode","episode_number","episode_run_time","freebase_id","freebase_mid","general","genres","guest_stars","homepage","images","imdb_id","languages","name","network","origin_country","original_name","original_title","overview","parts","place_of_birth","plot_keywords","production_code","production_companies","production_countries","releases","revenue","runtime","season","season_number","season_regular","spoken_languages","status","tagline","title","translations","tvdb_id","tvrage_id","type","video","videos"]
    },
    results: [
        {
            "vote_count":1030,
            "id":8374,
            "video":false,
            "vote_average":7.3,
            "title":"The Boondock Saints",
            "popularity":14.470389,
            "poster_path":"\/fg6fhyKg3vbdGtnf9Hq27Q5gS3r.jpg",
            "original_language":"en",
            "original_title":"The Boondock Saints",
            "genre_ids":[28,53,80],
            "backdrop_path":"\/zL6GpSqaEyRwNMkqKVXsROinzSa.jpg",
            "adult":false,
            "overview":"With a God-inspired moral obligation to act against evil, twin brothers Conner and Murphy set out to rid Boston of criminals. However, rather than working within the system, these Irish Americans decide to take swift retribution into their own hands.",
            "release_date":"1999-01-22"
        },{
            "vote_count":324,
            "id":22821,
            "video":false,
            "vote_average":5.8,
            "title":"The Boondock Saints II: All Saints Day",
            "popularity":10.150325,
            "poster_path":"\/hjU3caMd9O3h21e5kzJeUOAoy7h.jpg",
            "original_language":"en",
            "original_title":"The Boondock Saints II: All Saints Day",
            "genre_ids":[28,53,80],
            "backdrop_path":"\/cyoM4hIMHnIOWcLvfMlpmbpRer4.jpg",
            "adult":false,
            "overview":"Skillfully framed by an unknown enemy for the murder of a priest, wanted vigilante MacManus brothers Murphy and Connor must come out of hiding on a sheep farm in Ireland to fight for justice in Boston.",
            "release_date":"2009-11-24"
        },{
            "vote_count":0,
            "id":281602,
            "video":false,
            "vote_average":0,
            "title":"The Boondock Saints III",
            "popularity":1,
            "poster_path":null,
            "original_language":"en",
            "original_title":"The Boondock Saints III",
            "genre_ids":[],
            "backdrop_path":null,
            "adult":false,
            "overview":"The third installment of the Boondock Saints film series.",
            "release_date":""
        }
    ]
};

export default TestData;