import React from 'react';
import ResultList from '../src/controls/resultList/resultList';
import renderer from 'react-test-renderer';

it('ResultList renders correctly', () => {
	let resultList = (
		<ResultList
			items = {[]}
			emptyString = "No results"
		/>
	);
	
    let tree = renderer.create(resultList).toJSON();
    expect(tree).toMatchSnapshot();
});