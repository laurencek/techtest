import React from 'react';
import Pagination from '../src/controls/resultList/pagination';
import renderer from 'react-test-renderer';

it('Pagination renders correctly', () => {
	let pagination = (
		<Pagination
			page = { 1 }
			total = { 500 }
			totalPages = { 5 }
			changePage = {() => { null; }}
		/>
	);
	
    let tree = renderer.create(pagination).toJSON();
    expect(tree).toMatchSnapshot();
});