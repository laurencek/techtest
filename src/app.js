import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { ResultListContainer, SearchContainer } from './controls';

import { MovieRenderer, TvRenderer, PersonRenderer } from './renderers';

/**
 * Tha main entry point for the technical test app
 * @final
 */
class App extends Component {

	constructor() {
		super();

		this.movieApi = null;

		this.renderers = [];

		/** possible search types accepted by TheMovieDB */
		this.searchTypes = [ "Movie", "Tv", "Person" ];
		this.renderer = null;

		this.state = {
			configuration: null,
			items: [],
			page: 1,
			queryString: "",
			searchType: 0,
			total: 0,
			totalPages: 1
		};
	};

	/**
	 * Pulls in TheMovieDb libary that is included in the HTML and retrieves the configuration
	 */
	componentDidMount() {
		this.movieApi = theMovieDb;

		if (this.movieApi) {
			this.movieApi.configurations.getConfiguration( this.handleConfigurationSuccess, this.handleError );
		}		
	};

	/**
	 * @param {number} newPage page number, starting from 1
	 */
	doChangePage = (newPage) => {
		let page = newPage < 1
			? 1
			: (newPage > this.state.totalPages ? this.state.totalPages : newPage);

		this.setState({ page, items: [], total: 0, totalPages: 1 });

		this.doSearch( this.state.queryString, page, this.state.searchType );
	};

	/**
	 * @param {number} searchType index of current search type.
	 */
	doChangeSearchType = (searchType) => {
		this.setState({ searchType, page: 1, items: [], total: 0, totalPages: 1 });
		
		this.setRenderer(searchType);

		this.doSearch( this.state.queryString, this.state.page, searchType );
	};

	/**
	 * Perform the relevant call to The Movie Db
	 * @param {string} querystring
	 * @param {number} page
	 * @param {number} searchType
	 */
	doSearch = (queryString, page, searchType) => {
		/** creation of TheMovieDB search method from the searchType */
		let method = 'get' + this.searchTypes[searchType];

		if (this.movieApi) {
			this.movieApi.search[method]( { query: queryString, page }, this.handleSearchSuccess, this.handleError );
		} else {
			console.error("The Movie DB library is not loaded");
		}
	};

	/**
	 * @param {string} queryString string to search TheMovieDB with
	 */
	handleChange = (queryString) => {
		this.setState({ queryString, page: 1, items: [], total: 0, totalPages: 1 });

		this.doSearch( queryString, 1, this.state.searchType);
	};

	/**
	 * @param {string} data error data returned from TheMovieDB
	 */
	handleError = (data) => {
		console.error("Error retrieving data from The Movie DB", data);
	};

	/**
	 * @param {string} data data returned from TheMovieDB and put into the apps state
	 */
	handleSearchSuccess = (data) => {
		let { page, total_pages, total_results, results } = JSON.parse(data);
		
		this.setState({
			waiting: false,
			page,
			items: results,
			total: total_results,
			totalPages: total_pages
		});

		console.log(results);
	};

	/**
	 * Stores the configuration data from The Movie DB in the component state and
	 * initialises the different renderers
	 * @param {string} data configuration data returned from TheMovieDB
	 */
	handleConfigurationSuccess = (data) => {
		let configuration = JSON.parse(data);

		this.renderers = [new MovieRenderer(configuration), new TvRenderer(configuration), new PersonRenderer(configuration)];
		this.setRenderer(this.state.searchType);

		this.setState({ configuration });
	};

	setRenderer = (searchType) => {
		this.renderer = this.renderers[searchType];
	};

	render() {
		return (
			<div className = "app">
				<SearchContainer
					onChange = { this.handleChange }
					onChangeSearchType = { this.doChangeSearchType }
					searchType = { this.state.searchType }
					searchTypes = { this.searchTypes }
				/>
				{this.state.configuration &&
					<ResultListContainer
						doChangePage = { this.doChangePage }
						emptyString = { "No results" }
						items = { this.state.items }
						page = { this.state.page }
						renderer = { this.renderer }
						total = { this.state.total }
						totalPages = { this.state.totalPages }
					/>
				}
			</div>
		)
	}
};

export default App;