/**
 * @fileoverview the entry point for webpack to bundle up the js and connect the app to the HTML
 */

import React from 'react';
import ReactDOM from 'react-dom';

import App from './app';

import './stylesheets/main.scss';

ReactDOM.render(
	<App/>,
	document.getElementById('react-container')
);