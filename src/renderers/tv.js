import React from 'react';

/**
 * Renderer class for tv
 * @param {object} configuration object that stores the configuration from TheMovieDb
 * @final
 */
var Tv = function(configuration) {
    this.configuration = configuration;
};

/**
 * Produce JSX for a large view of a tv program
 * @param {object} tv data describing a tv program
 * @param {number} size index that corresponds to the size of the poster image in the configuration
 */
Tv.prototype.render = function(tv, size = 0) {
    let baseUrl = this.configuration.images.base_url,
        poster_size = this.configuration.images.poster_sizes[size];

    return (
        <span className = "item-contents item-type-tv">
            <span className = "tv-vote">
                { tv.vote_average }
            </span>
            <img src = { baseUrl + poster_size + '/' + tv.poster_path} />
            <h3 className = "tv-name">
                { tv.name }
            </h3>
            <p className = "tv-air-date">
                { tv.first_air_date }
            </p>
            <p className = "tv-overview">
                { tv.overview }
            </p>
        </span>
    );
};

export default Tv;