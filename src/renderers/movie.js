import React from 'react';

/**
 * Renderer class for movies
 * @param {object} configuration object that stores the configuration from TheMovieDb
 * @final
 */
var Movie = function(configuration) {
    this.configuration = configuration;
};

/**
 * Produce JSX for a large view of a movie
 * @param {object} movie data describing a movie
 * @param {number} size index that corresponds to the size of the poster image in the configuration
 */
Movie.prototype.render = function(movie, size = 0) {
    let baseUrl = this.configuration.images.base_url,
        poster_size = this.configuration.images.poster_sizes[size];

    return (
        <div className = "item-contents item-type-movie">
            <span className = "movie-vote">
                { movie.vote_average }
            </span>
            <img src = { baseUrl + poster_size + '/' + movie.poster_path} />
            <h3 className = "movie-title">
                { movie.title }
            </h3>
            <p className = "movie-release-date">
                { movie.release_date }
            </p>
            <p className = "movie-overview">
                { movie.overview }
            </p>
        </div>
    );
};

export default Movie;