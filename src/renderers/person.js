import React from 'react';

/**
 * Renderer class for person
 * @param {object} configuration object that stores the configuration from TheMovieDb
 * @final
 */
var Person = function(configuration) {
    this.configuration = configuration;
};

/**
 * Produce JSX for a large view of a person
 * @param {object} movie data describing a person
 * @param {number} size index that corresponds to the size of the poster image in the configuration
 */
Person.prototype.render = function(person, size = 0) {
    let baseUrl = this.configuration.images.base_url,
        poster_size = this.configuration.images.poster_sizes[size];

    return (
        <span className = "item-contents item-type-person">
            <img src = { baseUrl + poster_size + '/' + person.profile_path} />
            <h3 className = "person-name">
                { person.name }
            </h3>
            { this.renderKnownFor(person.known_for) }
        </span>
    );
};

/**
 * Produce JSX for known_for array
 * @param {Object[]} known_for array of movie/tv data the person has been known for
 * @return {Array|void} 
 */
Person.prototype.renderKnownFor = function(knownFor) {
    let returnArr = [];

    for(let i in knownFor) {
        returnArr.push(<li key = { i }>{ knownFor[i].title } - {knownFor[i].release_date }</li>);
    }

    if (returnArr.length) {
        return <ul className = "person-known-for">{ returnArr }</ul>;
    }
};

export default Person;