/**
 * @fileoverview index file to make importing renders easier
 */
export MovieRenderer from './movie.js'; 
export TvRenderer from './tv.js'; 
export PersonRenderer from './person.js'; 