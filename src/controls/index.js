/**
 * @fileoverview index file to make importing controls easier
 */
export ResultListContainer from "./resultList/resultListContainer";
export SearchContainer from "./search/searchContainer";