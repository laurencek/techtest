import React from "react";
import PropTypes from "prop-types";

/**
 * @param {object} configuration configuration object from The Movie DB
 * @param {Array} items array of result items from The Movie DB
 * @param {string} emptyString string to display if no items are returned
 */
const ResultList = ( { items, emptyString = "Nada", renderer = { render: () => "no renderer" } } ) => {

	let itemsJsx = items.map((item, index) => {
		return (
			<li
				className = "result-list-item"
				key = { index } >
                    { renderer.render(item) }
			</li>
		)
	});
	
	if (items.length) {
		return (
			<ul className = "result-list">
				{ itemsJsx }
			</ul>
		);
	}
	
	return <span className = "result-list-empty">{ emptyString }</span>;
};

ResultList.propTypes = {
	emptyString: PropTypes.string,
    items: PropTypes.array,
    renderer: PropTypes.object
};

export default ResultList;