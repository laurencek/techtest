import React from "react";
import PropTypes from "prop-types";

/**
 * @callback changePageCallback
 * @param {number} the page to change to
 */

/**
 * @param {number} page current page number
 * @param {number} total the number of items in all pages
 * @param {number} totalPages the number of pages
 * @param {changePageCallback} changePage the function called on the click of a JSX element
 * @return {Array} array of JSX elements
 */
const renderPages = ( { page, total, totalPages, doChangePage } ) => {

    let returnJsx = [],
        minPage = page < 4 ? 1 : page - 3,
        maxPage = page + 3 > totalPages ? totalPages : page + 3;

    for (let i = minPage; i <= maxPage; i++) {
        returnJsx.push(
            <li
                key = { i }
                className = { i === page ? "active" : "" }
                onClick = {() => { doChangePage(i); }}>
                    { i }
            </li>
        );
    }
	
	return returnJsx;
};

/**
 * @param {object} props properties to create a Pagination area
 */
const Pagination = ( props ) => {
    let renderedPages = renderPages( props );

    if (renderedPages.length) {
        return (
            <div className = "pagination">
                <ul className = "pages">
                    { renderedPages }
                </ul>
                <span className = "total-items">
                    { props.total } items
                </span>
                <span className = "total-pages">
                    { props.totalPages } pages
                </span>
            </div>
        );
    }
};

Pagination.propTypes = {
    doChangePage: PropTypes.func,
	page: PropTypes.number,
    total: PropTypes.number,
    totalPages: PropTypes.number
};

export default Pagination;