import React, { Component } from "react";
import PropTypes from "prop-types";

import Pagination from "./pagination";
import ResultList from "./resultList";

/**
 * Container for resultlist
 * @final
 */
export default class ResultListContainer extends Component {

	constructor(props) {
		super(props);
    };
    
	/**
	 * @return {Jsx} a div containing the resultlist
	 */
	render() {
		return (
            <div className = "result-list-container">
                { this.props.total > 0 &&
                    <Pagination
                        page = { this.props.page }
                        total = { this.props.total }
                        totalPages = { this.props.totalPages }
                        doChangePage = { this.props.doChangePage }
                    />
                }
                <ResultList
                    emptyString = { this.props.emptyString }
                    items = { this.props.items }
                    renderer ={ this.props.renderer }
                />
            </div>
		);
	}
};

ResultListContainer.propTypes = {
    doChangePage: PropTypes.func,
    emptyString: PropTypes.string,
    items: PropTypes.array,
    page: PropTypes.number,
    renderer: PropTypes.object,
    total: PropTypes.number,
    totalPages: PropTypes.number
};