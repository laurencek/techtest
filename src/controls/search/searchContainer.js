import React, { Component } from "react";
import PropTypes from "prop-types";

import SearchBox from "./searchBox";
import RadioButtonGroup from "./radioButtonGroup";

/**
 * Container for the SearchBox
 * @final
 */
export default class SearchContainer extends Component {

	constructor(props) {
		super(props);

		this.changeTimeout = null;
		this.changeDelay = 500;

		this.state = {
			value: ""
		};
	};

	/**
	 * Used when the searchbox value changes, includes a timeout to 
	 * reduce unnecessary calls
	 * @param {Object} e event object 
	 */
	onChange = (e) => {

		let value = e.target.value;

		clearTimeout(this.changeTimeout);

		this.changeTimeout = setTimeout( () => {
			this.props.onChange(value);
		}, this.changeDelay);

		this.setState({ value });		
	};

	/**
	 * @return {Jsx} a div containing the search box and label
	 */
	render() {
		return (
			<div className = "search-container">
				<label
					className = "search-box-label"
					htmlFor = "search-box">
						Search For
				</label>
				<SearchBox
					id = "search-box"
					value = { this.state.value }
					onChange = { this.onChange }
				/>
				<label className = "search-type-label">
					Type of
				</label>
				<RadioButtonGroup
					name = "searchType"
					value = { this.props.searchType }
					options = { this.props.searchTypes }
					onChange = { this.props.onChangeSearchType }
				/>
			</div>
		);
	}
}

SearchContainer.propTypes = {
	onChange: PropTypes.func,
	onChangeSearchType: PropTypes.func,
	searchType: PropTypes.number,
	searchTypes: PropTypes.array
};