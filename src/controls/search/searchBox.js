import React from "react";
import PropTypes from "prop-types";

/**
 * @param {Object} props properties to create a searchbox
 * @return {Jsx} html input element
 */
const SearchBox = ( props ) => (
    <input
        className = "search-box"
        id = { props.id }
        value = { props.value }
        onChange = { props.onChange }
    />
);

SearchBox.propTypes = {
    id: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
};

export default SearchBox;