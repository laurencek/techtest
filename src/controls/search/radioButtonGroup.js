import React from "react";
import PropTypes from "prop-types";

/**
 * @param {Object} props properties to create a group of radio buttons
 * @return {Jsx} div containing radio inputs and labels
 */
const RadioButtonGroup = ( props ) => {
    let returnArr = [];

    for (var i = 0, len = props.options.length; i < len; i++) {
        returnArr.push(
            <input
                key = { "radio" + i }
                type = "radio"
                name = { props.name }
                value = { i }
                checked = { props.value == i }
                id = { props.name + i }
                onChange = {(e) => { props.onChange(+e.target.value); }}
            />
        );
        returnArr.push(
            <label
                key = { "label" + i }
                htmlFor = { props.name + i }>
                    { props.options[i] }
            </label>
        );
    }

    return (
        <div className = "radio-button-group">
            {returnArr}
        </div>
    );
};

RadioButtonGroup.propTypes = {
    name: PropTypes.string,
    options: PropTypes.array,
    value: PropTypes.number,
    onChange: PropTypes.func
};

export default RadioButtonGroup;