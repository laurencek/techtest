module.exports = {
	mode: 'development',
	entry: {
		'bundle': './src/index.js',
	},
	devtool: 'source-map',
	output: {
		path: __dirname + '/public/assets',
		filename: '[name].js',
		publicPath: 'assets/'
	},
	devServer: {
		inline: true,
		contentBase: './public',
		port: 3000
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['latest', 'stage-0', 'react']
					}
				}
            },
			{
				test: /\.scss$/,
				use: [
					'style-loader',
					{ loader: 'css-loader', options: { importLoaders: 1 } },
					'sass-loader'
				]
			}
		]
	}
}